﻿using System.Data.Entity;
using Blog.Models;
using Blog.Models.Items;

namespace Blog.DAL
{
    public class BlogContext : DbContext
    {
        public BlogContext()
            : base("BlogContextConnection")
        {

        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Tag> Tags { get; set; }

    }
}