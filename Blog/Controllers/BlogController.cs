﻿using System.Web.Mvc;
using Blog.Models.Items;
using Blog.Models.Managers;

namespace Blog.Controllers
{
    public class BlogController : Controller
    {
        public ActionResult Index()
        {
            var posts = ManagersFactory.PostManagerInstance.GetPosts();
            return View(posts);
        }

        public ActionResult Post(int id)
        {
            Post post = ManagersFactory.PostManagerInstance.GetPost(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Blog/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = ManagersFactory.PostManagerInstance.GetPost(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Blog/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Blog/Create

        [HttpPost]
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                ManagersFactory.PostManagerInstance.AddPost(post);
                return RedirectToAction("Index");
            }

            return View(post);
        }

        //
        // GET: /Blog/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = ManagersFactory.PostManagerInstance.GetPost(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Blog/Edit/5

        [HttpPost]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                ManagersFactory.PostManagerInstance.UpdatePost(post);
                return RedirectToAction("Index");
            }
            return View(post);
        }

        //
        // GET: /Blog/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = ManagersFactory.PostManagerInstance.GetPost(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Blog/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ManagersFactory.PostManagerInstance.RemovePost(id);
            return RedirectToAction("Index");
        }
    }
}