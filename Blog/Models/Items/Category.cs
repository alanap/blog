﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Blog.Models.Items
{
    public class Category
    {
        [Key]
        public int Id
        { get; set; }

        public string Name
        { get; set; }

        public string UrlSlug
        { get; set; }

        public string Description
        { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}