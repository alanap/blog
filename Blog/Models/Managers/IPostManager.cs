﻿using System.Collections.Generic;
using Blog.Models.Items;

namespace Blog.Models.Managers
{
    public interface IPostManager
    {
        void AddPost(Post post);
        void RemovePost(Post post);
        void RemovePost(int id);
        void UpdatePost(Post post);

        IEnumerable<Post> GetPosts();
        Post GetPost(int id);
    }
}
