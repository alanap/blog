﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Blog.DAL;
using Blog.Models.Items;

namespace Blog.Models.Managers
{
    public class PostManager : IPostManager
    {
        private readonly BlogContext _database = new BlogContext();

        public void AddPost(Post post)
        {
            _database.Posts.Add(post);
            _database.SaveChanges();
        }

        public void RemovePost(Post post)
        {
            _database.Posts.Remove(post);
            _database.SaveChanges();
        }

        public void RemovePost(int id)
        {
            Post post = GetPost(id);
            RemovePost(post);
        }

        public void UpdatePost(Post post)
        {
            _database.Entry(post).State = EntityState.Modified;
            _database.SaveChanges();
        }

        public IEnumerable<Post> GetPosts()
        {
            using (var context = new BlogContext())
            {
                return context.Posts.ToList();
            }
        }

        public Post GetPost(int id)
        {
            return _database.Posts.Find(id);
        }
    }
}