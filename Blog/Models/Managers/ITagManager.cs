﻿using System.Collections.Generic;

namespace Blog.Models.Managers
{
    public interface ITagManager
    {
        Dictionary<string, int> GetTags();
    }
}
