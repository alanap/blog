﻿namespace Blog.Models.Managers
{
    public class ManagersFactory
    {
        private static IPostManager _entryManager;
        private static ITagManager _tagManager;

        public static IPostManager PostManagerInstance
        {
            get { return _entryManager ?? (_entryManager = new PostManager()); }
        }

        public static ITagManager TagManagerInstance
        {
            get { return _tagManager ?? (_tagManager = new TagManager()); }
        }
    }
}